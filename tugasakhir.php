<br/><br/><br/><br/><br/>
<div class="container">
<div class="row">
    <div class="table-responsive">
<table id="dataTable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Nim</th>
				<th>Nama</th>
				<th>Judul</th>
				<th>Jurusan</th>
				<th>Pembimbing</th>
				
			</tr>
		</thead>
		<tbody>
		<?php
			include "access/db.php";
			$sql = "SELECT `no`, `nim`, `nama`, `jurusan`, `judul`, `pembimbing`, `tahun`, `TUGAS` FROM `skripsi` WHERE `TUGAS`='TUGAS AKHIR'";
			$result = $db->query($sql);
			$no=1;
			while($row = mysqli_fetch_assoc($result)){ ?>
			<tr>
				<td><?=$no;?></td>
				<td><?=$row['nim'];?></td>
				<td><?=$row['nama'];?></td>
				<td><?=$row['judul'];?></td>
				<td><?=$row['jurusan'];?></td>
				<td><?=$row['pembimbing'];?></td>
				
			</tr>
			<?php
			$no++;
			  }
			?>
		</tbody>
	</table>
	<script type="text/javascript">
       $(document).ready(function() {
			$('#dataTable').dataTable();
		} );

    </script>
</div></div></div>
	