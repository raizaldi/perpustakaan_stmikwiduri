<?php
include '../access/db.php';
error_reporting(0);
session_start();
if(isset($_SESSION['username']) || $_SESSION['level']=="administrator"){
	header('location:index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Admin Perpustakaan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="assets/css/semantic.css">
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
</head>

<body id="loginBackground">
    <div class="ui container">
        <div class="ui centered stackable grid">
            <div class="six wide column">
                <div class="ui segments" align="center">
                    <div class="ui inverted blue padded segment">
                        <span class="ui header">Selamat datang dan silahkan login untuk dapat memasuki area admin</span>
                    </div>
                    <div class="ui segment">
                        <form class="ui form" action="" method="POST">
                            <div class="field">
                                <div class="ui left icon input">
                                    <input type="text" name="username" placeholder="Username">
                                    <i class="user icon"></i>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui left icon input">
                                    <input type="password" name="password" placeholder="Password">
                                    <i class="lock icon"></i>
                                </div>
                            </div>
                            
                            <button class="ui big fluid blue icon button" type="submit" name="submit">Sign In <i class="sign in icon"></i></button>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/semantic.min.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
</body>
</html>



<?php
include '../access/db.php';
if(isset($_POST['submit'])){
	$username = $_POST['username'];
	$password = md5($_POST['password']);
	$sql = "SELECT `id`, `username`, `namapetugas`, `password`, `level` FROM `petugas` WHERE `username`='$username' and`password`='$password'";
	$result = $db->query($sql);
	if ($result->num_rows > 0) {
		while($row = $result->fetch_assoc()) {
			$username = $row['username'];
			$password = $row['password'];
			$level    = $row['level'];
			$namapetugas   = $row['namapetugas'];
			if($username==$username && $password==$password){
				session_start();
				$_SESSION['username']   = $username;
				$_SESSION['namapetugas']= $namapetugas;
                                $_SESSION['level']      = $level;
				if($level=='administrator'){
					header("location:index.php");
				}
				else if($level=='mahasiswa'){
					header("location:../index.php");
				}
                                else if($level=='dosen'){
					header("location:../index.php");
				}
			}
		}
    }
	
	}
?>