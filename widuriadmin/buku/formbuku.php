<?php
include '../access/db.php';
error_reporting(0);
session_start();
if(!isset($_SESSION['username']) || $_SESSION['level']!="administrator"){
	header('location:login.php');
}
?>

<div class="ui two column stackable grid ui form">
    <div class="column">
        <div class="ui segments">
            <div class="ui segment"><h3>Form Buku</h3></div>
            <div class="ui segment">
                <form action=""  method="POST" enctype="multipart/form-data">
                <div class="field">
                    <div class="ui fluid input">
                        <input type="text" name="kodebuku" placeholder="Kode Buku">
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid input">
                        <input type="text" name="judulbuku" placeholder="Judul Buku">
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid input">
                         <input type="text" name="pengarang" placeholder="Pengarang">
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid input">
                         <input type="text" name="penerbit" placeholder="Penerbit">
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid input">
                        <select name='kategori' placeholder="kategori">
                                <option>Pilih Kategori</option>
                                <option name='komputer'>Komputer</option>
                                <option name='sosial'>Sosial</option>
                                <option name='politik'>Politik</option>
                        </select>
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid input">
                         <input type="text" name="tahun" placeholder="Tahun">
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid input">
                         <input type="text" name="jumlahhalaman" placeholder="Jumlah halaman">
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid input">
                         <input type="text" name="stok" placeholder="Stok">
                    </div>
                </div>
                <div class="field">
                    <div class="ui fluid input">
                         <input type="text" name="rak" placeholder="Rak">
                    </div>
                </div>
                
                <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
                </form>
            </div>
            
        </div>
    </div>
</div>
<?php
include '../access/db.php';
if(isset($_POST['submit'])){

$kodebuku    = $_POST['kodebuku'];
$judulbuku     = $_POST['judulbuku'];
$pengarang     = $_POST['pengarang'];
$penerbit     = $_POST['penerbit'];
$kategori     = $_POST['kategori'];
$tahun     = $_POST['tahun'];
$jumlahhalaman     = $_POST['jumlahhalaman'];
$stok     = $_POST['stok'];
$rak     = $_POST['rak'];

    
    $sql = "INSERT INTO `buku`(`kodebuku`, `judulbuku`, `pengarang`, `penerbit`, `kategori`, `tahun`, `jumlahhalaman`, `stok`, `Rak`) VALUES ('$kodebuku','$judulbuku','$pengarang','$penerbit','$kategori','$tahun','$jumlahhalaman','$stok','$rak')";
   
    $res = $db->query($sql);
    if($res){
      header('location:?module=buku');
    }
}
?>
