<?php
include '../access/db.php';
error_reporting(0);
session_start();
if(!isset($_SESSION['username']) || $_SESSION['level']!="administrator"){
	header('location:login.php');
}
?>
<div class="container">
<div class="row">
<div class="table-responsive">
<table id="dataTable" class="table table-striped table-bordered">
		<thead>
      <div> <a href="?module=buku/new" class="btn btn-info btn-sm" role="button">Tambah</a></div>
			<tr>
				<th>Kode Buku</th>
				<th>Judul Buku</th>
				<th>Pengarang</th>
        <th>penerbit</th>
        <th>kategori</th>
        <th>tahun</th>
        <th>jumlah halaman</th>
        <th>stok</th>
        <th>rak</th>
       <th>Aksi</th>
			</tr>
		</thead>
		<tbody><br/><br/><br/>
		<?php
			include "../access/db.php";
			$sql = "SELECT `id`, `kodebuku`, `judulbuku`, `pengarang`, `penerbit`, `kategori`, `tahun`, `jumlahhalaman`, `stok`, `Rak` FROM `buku`";
			$result = $db->query($sql);
			while($row = mysqli_fetch_assoc($result)){ ?>
			<tr>
					<td><?=$row['kodebuku'];?></td>
					<td><?=$row['judulbuku'];?></td>
	        <td><?=$row['pengarang'];?></td>
					<td><?=$row['penerbit'];?></td>
	        <td><?=$row['kategori'];?></td>
					<td><?=$row['tahun'];?></td>
	        <td><?=$row['jumlahhalaman'];?></td>
					<td><?=$row['stok'];?></td>
          <td><?=$row['Rak'];?></td>
          <td>
              <a href="?module=buku/edit&u=<?=$row['kodebuku'];?>" class="btn btn-primary btn-sm" role="button">Ubah</a>
              <a href="?module=buku/hapus&h=<?=$row['kodebuku'];?>" class="btn btn-danger btn-sm" role="button">Hapus</a>
          </td>
			</tr>
			<?php
			$no++;
			  }
			?>
		</tbody>
	</table>
</div>
	<script type="text/javascript">
        $(document).ready(function () {
            var table = $('#dataTable').dataTable();
            var tableTools = new $.fn.dataTable.TableTools(table, {
                'aButtons': [
                    {
                        'sExtends': 'xls',
                        'sButtonText': 'Save to Excel',
                        'sFileName': 'Data.xls'
                    },
                    {
                        'sExtends': 'print',
                        'bShowAll': true,
                    },
                    {
                        'sExtends': 'pdf',
                        'bFooter': false
                    },
                    'copy',
                    'csv'
                ],
                'sSwfPath': '//cdn.datatables.net/tabletools/2.2.4/swf/copy_csv_xls_pdf.swf'
            });
            $(tableTools.fnContainer()).insertBefore('#dataTable_wrapper');
        });
    </script>
</div></div>
