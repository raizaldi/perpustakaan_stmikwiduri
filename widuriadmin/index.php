<?php
include '../access/db.php';
error_reporting(0);
session_start();
if(!isset($_SESSION['username']) || $_SESSION['level']!="administrator"){
	header('location:login.php');
}
?>
<!DOCTYPE html>
<html>
<head>
    <title><?= $_SESSION['level'] ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" type="text/css" href="assets/css/semantic.css">
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <link rel="stylesheet" type="text/css" href="assets/css/animate.css">

     <!-- Bootstrap core CSS -->
    <link href="../assets/bootstrap.min.css" rel="stylesheet"><link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/tabletools/2.2.4/css/dataTables.tableTools.css" />
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../assets/js/ie-emulation-modes-warning.js"></script>
    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/9dcbecd42ad/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/tabletools/2.2.4/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="assets/js/semantic.min.js"></script>
    <script type="text/javascript" src="assets/js/nicescroll.min.js"></script>



</head>

<body>
    <div class="ui fixed top menu">
       <a href="index.php" data-toggle="slide" class="item"><h3>Selamat datang <?= $_SESSION['namapetugas'] ?> </h3></a>
        <div class="menu right">
        </div>
    </div>
    <div class="pusher">
        <div class="ui fluid container">
            <div class="ui two column grid">
                <div class="side menu" id="sideMenu">
                    <div align="center" class="profile">
                        <br>
                        <a href=""><img src="../assets/logowiduri.png" alt="Kampus Widuri" class="ui circular image" width="160px"></a>
                        <div class="ui white header">Kampus Widuri</div>
                        <div class="ui divider"></div>
                    </div>
                    <div class="ui fluid vertical menu" id="verticalMenu">
                        <a href="?module=home" class="item"><i class="dashboard icon"></i> <span>Dashboard</span></a>
                        <a href="?module=ebook" class="item"><i class="book icon"></i> <span>Ebook</span></a>
                        <a href="?module=buku" class="item"><i class="table icon"></i> <span>List Buku</span></a>
                        <a href="?module=skripsi" class="item"><i class="bar chart icon"></i> <span>Skripsi/Tugas Akhir dan KP</span></a>
                        <a href="?module=user" class="item"><i class="block layout icon"></i> <span>Membuat User</span></a>
                        <a href="logout.php" class="item"><i class="block layout icon"></i> <span>Logout</span></a>
                    </div>
                </div>
                <div class="sixteen wide column" id="content">
                  <?php
                        if(!isset($_GET['module'])){
                          include "home.php";
                        }
                        else if($_GET['module']=='home'){
                          include "home.php";
                        }
												//ini untuk ebook
                        else if($_GET['module']=='ebook'){
                          include "ebook/ebook.php";
                        }
												else if($_GET['module']=='ebook/new'){
                          include "ebook/formebook.php";
                        }
												else if($_GET['module']=='ebook/edit'){
                          include "ebook/ebooku.php";
                        }
												else if($_GET['module']=='ebook/hapus'){
                          include "ebook/ebookh.php";
                        }
												//ini untuk buku
												else if($_GET['module']=='buku'){
                          include "buku/buku.php";
                        }
												else if($_GET['module']=='buku/new'){
                          include "buku/formbuku.php";
                        }
												else if($_GET['module']=='buku/edit'){
                          include "buku/bukuku.php";
                        }
												else if($_GET['module']=='buku/hapus'){
                          include "buku/bukuh.php";
                        }

												//ini untuk skripsi
												else if($_GET['module']=='skripsi'){
													include "skripsi/skripsi.php";
												}
												else if($_GET['module']=='skripsi/new'){
													include "skripsi/formskripsi.php";
												}
												else if($_GET['module']=='skripsi/edit'){
													include "skripsi/skripsiu.php";
												}
												else if($_GET['module']=='skripsi/hapus'){
													include "skripsi/skripsih.php";
												}

												//ini untuk user
												else if($_GET['module']=='user'){
													include "user/user.php";
												}
												else if($_GET['module']=='user/new'){
													include "user/formuser.php";
												}
												else if($_GET['module']=='user/edit'){
													include "user/useru.php";
												}
												else if($_GET['module']=='user/hapus'){
													include "user/userh.php";
												}
                        ?>



                </div>
            </div>
        </div>
    </div>


</body>
</html>
