<?php
include '../access/db.php';
error_reporting(0);
session_start();
if(!isset($_SESSION['username']) || $_SESSION['level']!="administrator"){
	header('location:login.php');
}
?>
<br/><br/><br/><br/><br/>
<div class="container">
<div class="row">
<table id="dataTable" class="table table-striped table-bordered">
		<thead>
                    <div> <a href="?module=ebook/new" class="btn btn-info btn-sm" role="button">Tambah</a></div>
			<tr>
				<th>No</th>
				<th>Judul Buku</th>
				<th>Nama Penulis</th>
                               <th>Aksi</th>
			</tr>
		</thead>
		<tbody>

                <br/><br/><br/>
		<?php
			include "../access/db.php";
			$sql = "SELECT `id_buku`, `judul_buku`, `nama_penulis`, `tanggal_upload`, `file` FROM `ebook`";
			$result = $db->query($sql);
			$no=1;
			while($row = mysqli_fetch_assoc($result)){ ?>
			<tr>
				<td><?=$no;?></td>
				<td><?=$row['judul_buku'];?></td>
				<td><?=$row['nama_penulis'];?></td>
                                <td>
                                    <a href="?module=ebook/edit&u=<?=$row['id_buku'];?>" class="btn btn-primary btn-sm" role="button">Ubah</a>
                                    <a href="?module=ebook/hapus&h=<?=$row['id_buku'];?>" class="btn btn-danger btn-sm" role="button">Hapus</a>
                                </td>
			</tr>
			<?php
			$no++;
			  }
			?>
		</tbody>
	</table>

	<script type="text/javascript">
        $(document).ready(function () {
            var table = $('#dataTable').dataTable();
            var tableTools = new $.fn.dataTable.TableTools(table, {
                'aButtons': [
                    {
                        'sExtends': 'xls',
                        'sButtonText': 'Save to Excel',
                        'sFileName': 'Data.xls'
                    },
                    {
                        'sExtends': 'print',
                        'bShowAll': true,
                    },
                    {
                        'sExtends': 'pdf',
                        'bFooter': false
                    },
                    'copy',
                    'csv'
                ],
                'sSwfPath': '//cdn.datatables.net/tabletools/2.2.4/swf/copy_csv_xls_pdf.swf'
            });
            $(tableTools.fnContainer()).insertBefore('#dataTable_wrapper');
        });
    </script>
</div></div>
