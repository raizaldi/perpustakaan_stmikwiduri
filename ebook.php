<br/><br/><br/><br/><br/>
<div class="container">
<div class="row">
<div class="table-responsive">
<table id="dataTable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Judul Buku</th>
				<th>Nama Penulis</th>
				<th>Aksi</th>
				
			</tr>
		</thead>
		<tbody>
		<?php
			include "access/db.php";
			$sql = "SELECT `id_buku`, `judul_buku`, `nama_penulis`, `tanggal_upload`, `file` FROM `ebook`";
			$result = $db->query($sql);
			$no=1;
			while($row = mysqli_fetch_assoc($result)){ ?>
			<tr>
				<td><?=$no;?></td>
				<td><?=$row['judul_buku'];?></td>
				<td><?=$row['nama_penulis'];?></td>
                                <td><a href="index.php?module=lihatebook&download=<?=$row['id_buku'];?> " class='btn btn-info btn-sm' role='button'>Download</a></td>
			</tr>
			<?php
			$no++;
			  }
			?>
		</tbody>
	</table>
    
	<script type="text/javascript">
       $(document).ready(function() {
			$('#dataTable').dataTable();
		} );

    </script>
</div></div></div>