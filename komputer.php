<br/><br/><br/><br/><br/>
<div class="container">
<div class="row">
    <div class="table-responsive">
<table id="dataTable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Judul Buku</th>
				<th>Pengarang</th>
				<th>Penerbit</th>
				<th>Tahun</th>
				<th>Aksi</th>
				
			</tr>
		</thead>
		<tbody>
		<?php
			include "access/db.php";
			$sql = "SELECT `id`, `kodebuku`, `judulbuku`, `pengarang`, `penerbit`, `kategori`, `tahun`, `jumlahhalaman`, `stok` FROM `buku` where `kategori`='Komputer'";
			$result = $db->query($sql);
			$no=1;
			while($row = mysqli_fetch_assoc($result)){ ?>
			<tr>
				<td><?=$no;?></td>
				<td><?=$row['judulbuku'];?></td>
				<td><?=$row['pengarang'];?></td>
				<td><?=$row['penerbit'];?></td>
				<td><?=$row['tahun'];?></td>
				<td><a href="index.php?module=buku&cek=<?=$row['kodebuku']?>" role="button""><b>Lihat</a></td>
				
			</tr>
			<?php
			$no++;
			  }
			?>
		</tbody>
	</table>
    <script type="text/javascript">
       $(document).ready(function() {
			$('#dataTable').dataTable();
		} );

    </script>
</div></div></div>
	


